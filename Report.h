#include <iostream>
#include <string>
struct Report
{
    int processedFiles;
    int jsSuspiciousCount;
    int unixSuspiciousCount;
    int macSuspiciousCount;
    int errors;
    float runtime;
};

void printReport(Report *reportPtr)
{
    std::cout << "Processed FIles:" << reportPtr->processedFiles << std::endl;
    std::cout << "JS detects:" << reportPtr->jsSuspiciousCount << std::endl;
    std::cout << "UNIX detects:" << reportPtr->unixSuspiciousCount << std::endl;
    std::cout << "MacOS detects:" << reportPtr->macSuspiciousCount << std::endl;
    std::cout << "errors:" << reportPtr->errors << std::endl;
    std::cout << "runtime:" << reportPtr->runtime << " sec" << std::endl;
}

const int REPORTSIZE = sizeof(Report);