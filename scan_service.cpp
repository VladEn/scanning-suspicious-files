#include <dirent.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <functional>
#include <iostream>
#include <thread>
#include <vector>

#include "FunctionPool.h"
#include "Suspicious.h"
#include "Report.h"

#define NAMEDPIPE_NAME "/tmp/named_pipe"
#define BUFSIZE 255

using namespace std;

bool BoyerMooreHorspoolAlg(std::string inputString, std::string pattern,
                           std::map<char, int> &table);

static mutex mtx;

static int processedFiles;
static int errors;

vector<string> GetDirFiles(string dirPath)
{
    DIR *dir = opendir(dirPath.c_str());
    vector<string> arr;
    if (dir == nullptr)
        throw invalid_argument("Invalid path.");
    struct dirent *tmpdir;
    while ((tmpdir = readdir(dir)) != nullptr)
    {
        arr.push_back(tmpdir->d_name);
    }
    closedir(dir);
    return arr;
}

void checkFile(string filePath, vector<Suspicious *> *suspectList)
{
    mtx.lock();
    // cout << "this thread id = " << this_thread::get_id() << endl;
    processedFiles++;
    mtx.unlock();
    string tempstr;
    ifstream fin(filePath, ifstream::in);
    if (!fin.is_open())
    {
        errors++;
        return;
    }
    while (getline(fin, tempstr))
    {
        for (auto suspect : *suspectList)
        {
            if (BoyerMooreHorspoolAlg(tempstr, suspect->getSuspicousString(),
                                      *suspect->getTablePtr()))
            {
                mtx.lock();
                suspect->increaseDetectsCount();
                mtx.unlock();
                return;
            }
        }
    }
    fin.close();
}

void doWork(string path)
{
    int pipe_fd, length;
    auto startTime = chrono::high_resolution_clock::now();
    const auto processor_count = thread::hardware_concurrency();
    auto namesOfFIles = GetDirFiles(path);
    Suspicious *jsSuspicious = new Suspicious("JS", "<script>evil_script()</script>");
    Suspicious *unixSuspicious = new Suspicious("UNIX", "rm -rf ~/Documents");
    Suspicious *macosSuspicious = new Suspicious(
        "MacOs",
        "system(\"launchctl load /Library/LaunchAgents/com.malware.agent\")");
    vector<Suspicious *> suspectList = {jsSuspicious, unixSuspicious,
                                        macosSuspicious};

    FunctionPool funcPool(&suspectList);
    vector<thread> threadPool;

    for (int i = 0; i < processor_count; i++)
        threadPool.push_back(thread(&FunctionPool::infinite_loop_func, &funcPool));

    for (auto filename : namesOfFIles)
        if (filename != ".." && filename != ".")
            funcPool.push(checkFile, string(path + "/" + filename));

    funcPool.done();

    for (unsigned int i = 0; i < threadPool.size(); i++)
        threadPool[i].join();

    auto endTime = chrono::high_resolution_clock::now();
    chrono::duration<float> duration = endTime - startTime;

    Report *reportPtr = new Report();
    reportPtr->processedFiles = processedFiles;
    reportPtr->jsSuspiciousCount = jsSuspicious->getDetectsCount();
    reportPtr->unixSuspiciousCount = unixSuspicious->getDetectsCount();
    reportPtr->macSuspiciousCount = macosSuspicious->getDetectsCount();
    reportPtr->errors = errors;
    reportPtr->runtime = duration.count();

    char *buf = new char[REPORTSIZE];
    memcpy(buf, reportPtr, REPORTSIZE);

    if ((pipe_fd = open(NAMEDPIPE_NAME, O_WRONLY)) <= 0)
    {
        perror("checkfile() open namedpipe error");
        return;
    }
    if ((length = write(pipe_fd, buf, REPORTSIZE)) <= 0)
        perror("checkfile() write to namedpipe error");
    close(pipe_fd);
    delete (reportPtr, buf);
    processedFiles = 0;
    errors = 0;
}

int main(void)
{
    setlocale(LC_ALL, "russian");

    int pipe_fd, length;
    char buf[BUFSIZE];

    mkfifo(NAMEDPIPE_NAME, 0777);
    std::cout << "== Scan service is started ==" << endl;
    while (true)
    {
        if ((pipe_fd = open(NAMEDPIPE_NAME, O_RDONLY)) <= 0)
        {
            perror("open");
            return 1;
        }
        if ((length = read(pipe_fd, buf, BUFSIZE)) <= 0)
            perror("read error!");
        close(pipe_fd);
        if (strcmp(buf, "quit") == 0)
        {
            remove(NAMEDPIPE_NAME);
            return 0;
        }
        doWork((string)buf);
    }
}
