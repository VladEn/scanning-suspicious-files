#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include <iostream>
#include <string>

#include "Report.h"

#define NAMEDPIPE_NAME "/tmp/named_pipe"
#define BUFSIZE 255

int main(int argc, char **argv)
{
    if (!mkfifo(NAMEDPIPE_NAME, 0777))
    {
        std::cout << "Start the service application first!" << std::endl;
        remove(NAMEDPIPE_NAME);
        return 0;
    }
    if (argc != 2)
        throw std::invalid_argument("Incorrect count of arguments.");

    int pipe_fd, length;
    std::string str = argv[1];
    char path[BUFSIZE];
    char *buf = new char[REPORTSIZE];
    memset(path, '\0', BUFSIZE);
    strcpy(path, str.c_str());

    if ((pipe_fd = open(NAMEDPIPE_NAME, O_WRONLY)) <= 0)
    {
        perror("open");
        return 1;
    }
    if ((length = write(pipe_fd, path, BUFSIZE)) <= 0)
        perror("write error!");
    close(pipe_fd);
    if (strcmp(path, "quit") == 0)
        return 0;

    if ((pipe_fd = open(NAMEDPIPE_NAME, O_RDONLY)) <= 0)
    {
        perror("open error!!!");
        return 1;
    }
    if ((length = read(pipe_fd, buf, REPORTSIZE)) <= 0)
        perror("read error");
    close(pipe_fd);
    Report *reportPtr = new Report();
    memcpy(reportPtr, buf, REPORTSIZE);
    printReport(reportPtr);
    delete (reportPtr);
}
