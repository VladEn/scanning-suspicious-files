CXX= g++
CXXFLAGS= -O2



SCAN_UTIL = scan_service
UTIL = scan_util
REPORT = Report
BMHALG = BoyerMooreHorspool
SUSP = Suspicious
FUNCPOOL = FunctionPool

all: $(SCAN_UTIL) $(UTIL)

$(SCAN_UTIL): $(SCAN_UTIL).o $(BMHALG).o $(SUSP).o $(FUNCPOOL).o
	$(CXX)  $^ -o $@ $(CXXFLAGS) -lpthread

$(UTIL): $(UTIL).o
	$(CXX)  $(UTIL).o -o $@

$(UTIL).o: $(UTIL).cpp
	@$(CXX)  -c $*.cpp -o $@

$(SCAN_UTIL).o: $(SCAN_UTIL).cpp
	@$(CXX)  -c $*.cpp -o $@ $(CXXFLAGS)
	
$(BMHALG).o: $(BMHALG).cpp
	@$(CXX)  -c $*.cpp -o $@ $(CXXFLAGS)

$(SUSP).o: $(SUSP).cpp
	@$(CXX) -c $*.cpp -o $@
	
$(FUNCPOOL).o: $(FUNCPOOL).cpp
	@$(CXX) -c $*.cpp -o $@

clean:
	@rm -f *.o *.
	@ls | grep -v "\." | grep -v makefile | xargs rm -f
	
cleanobj:
	@rm -f *.o
