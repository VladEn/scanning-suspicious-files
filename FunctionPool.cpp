#include "FunctionPool.h"

FunctionPool::FunctionPool(vector<Suspicious *> *suspectList)
    : m_function_queue(), m_lock(), m_data_condition(), m_accept_functions(true), suspectList(suspectList)
{
}

FunctionPool::~FunctionPool() {}

void FunctionPool::push(
    function<void(string, vector<Suspicious *> *suspectList)> func,
    string checkedLine)
{
    unique_lock<mutex> lock(m_lock);
    m_function_queue.push(func);
    m_string_Queue.push(checkedLine);
    lock.unlock();
    m_data_condition.notify_one();
}

void FunctionPool::done()
{
    unique_lock<mutex> lock(m_lock);
    m_accept_functions = false;
    lock.unlock();
    m_data_condition.notify_all();
}

void FunctionPool::infinite_loop_func()
{
    function<void(string, vector<Suspicious *> * suspectList)> func;
    string suspString;
    while (true)
    {
        {
            unique_lock<mutex> lock(m_lock);
            m_data_condition.wait(lock, [this]()
                                  { return !m_function_queue.empty() || !m_accept_functions; });
            if (!m_accept_functions && m_function_queue.empty())
                return;
            func = m_function_queue.front();
            m_function_queue.pop();
            suspString = m_string_Queue.front();
            m_string_Queue.pop();
        }
        func(suspString, this->suspectList);
    }
}