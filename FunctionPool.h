#pragma once

#include "Suspicious.h"
#include <condition_variable>
#include <atomic>
#include <functional>
#include <mutex>
#include <queue>
#include <string>
#include <thread>

using namespace std;

class FunctionPool
{
private:
    queue<function<void(string, vector<Suspicious *> *)>> m_function_queue;
    mutex m_lock;
    condition_variable m_data_condition;
    atomic<bool> m_accept_functions;
    vector<Suspicious *> *suspectList;
    queue<string> m_string_Queue;

public:
    FunctionPool(vector<Suspicious *> *);
    ~FunctionPool();
    void push(function<void(string, vector<Suspicious *> *suspectList)>, string);
    void done();
    void infinite_loop_func();
};
