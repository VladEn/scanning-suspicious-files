#pragma once

#include <iostream>
#include <map>
#include <string>

std::map<char, int> *createMaskTable(std::string pattern);

using namespace std;

class Suspicious
{
private:
    string name;
    string suspiciousString;
    int detectsCount;
    map<char, int> *table;

public:
    Suspicious(string, string);
    string getSuspicousString();
    int getDetectsCount();
    void increaseDetectsCount();
    map<char, int> *getTablePtr();
    void printDetects();
    ~Suspicious();
    void print();
};