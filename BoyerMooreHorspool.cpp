#include <map>
#include <string>

std::map<char, int> *createMaskTable(std::string pattern)
{
    int length = pattern.length();
    std::map<char, int> *table = new std::map<char, int>();
    for (int i = length - 2; i >= 0; i--)
    {
        if (table->count(pattern[i]) > 0)
            continue;
        else
            table->emplace(pattern[i], length - i - 1);
    }
    if (table->count(pattern[length - 1]) < 1)
        table->emplace(pattern[length - 1], length);
    return table;
}

bool BoyerMooreHorspoolAlg(std::string inputString, std::string pattern,
                           std::map<char, int> &table)
{
    bool patternFound = false;
    bool overlap = false;
    int shift = 0;
    int patternlength = pattern.length();

    while (!patternFound)
    {
        overlap = false;
        for (int i = patternlength - 1; i >= 0; i--)
        {
            if (i + shift >= inputString.length())
                return false;
            if (pattern[i] == inputString[i + shift])
            {
                overlap = true;
                if (i == 0)
                    return true;
                continue;
            }
            else
            {
                if (overlap)
                {
                    shift += table[pattern[patternlength - 1]];
                }
                else
                {
                    if (table.count(inputString[i + shift]) > 0)
                        shift += table[inputString[i + shift]];
                    else
                        shift += patternlength;
                }
                break;
            }
            patternFound = true;
        }
    }
    return true;
}