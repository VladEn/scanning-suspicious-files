#include "Suspicious.h"

Suspicious::Suspicious(std::string name, std::string str)
{
    this->suspiciousString = str;
    this->name = name;
    detectsCount = 0;
    table = createMaskTable(suspiciousString);
}

std::string Suspicious::getSuspicousString() { return suspiciousString; }
int Suspicious::getDetectsCount() { return detectsCount; }

void Suspicious::increaseDetectsCount() { detectsCount++; }

std::map<char, int> *Suspicious::getTablePtr() { return table; }

void Suspicious::printDetects() { cout << name << " detects :\t" << detectsCount << endl; }

Suspicious::~Suspicious() {}
